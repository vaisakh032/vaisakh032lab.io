---
layout: inner
title: Skills
permalink: /skills/
---
<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
::selection{
  color: #fff;
  background: #6665ee;
}
.clicker {
cursor:pointer;
}
.hiddendiv{
display:none;
}
.clicker:focus + .hiddendiv{
display:block;
transition:display 1s linear;
}
.skill-bars{
  position: relative;
  top: 0;
  padding: 25px 30px;
  width: 100%;
  background: #fff;
  box-shadow: 5px 5px 20px rgba(0,0,0,0.2);
  border-radius: 10px;
  margin-left: auto;
  margin-right: auto;
  transition: top ease 0.5s, box-shadow 0.3s ease-in-out;
}
.skill-bars:hover{
  top: -10px;
  box-shadow: 8px 5px 20px rgba(0,0,0,0.4);
}
.skill-bars .bar{
  margin: 20px 0;
}
.skill-bars .bar:first-child{
  margin-top: 0px;
}
.skill-bars .bar .info{
  margin-bottom: 5px;
}
.skill-bars .bar .info span{
  font-weight: 500;
  font-size: 17px;
  opacity: 0;
  animation: showText 0.5s 1s linear forwards;
}
@keyframes showText {
  100%{
    opacity: 1;
  }
}
.skill-bars .bar .progress-line{
  height: 10px;
  width: 100%;
  background: #f0f0f0;
  position: relative;
  transform: scaleX(0);
  transform-origin: left;
  border-radius: 10px;
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.05),
              0 1px rgba(255,255,255,0.8);
  animation: animate 1s cubic-bezier(1,0,0.5,1) forwards;
}
@keyframes animate {
  100%{
    transform: scaleX(1);
  }
}
.bar .progress-line span{
  height: 100%;
  position: absolute;
  border-radius: 10px;
  transform: scaleX(0);
  transform-origin: left;
  background: var(--backgroundColour);
  animation: animate 1s 1s cubic-bezier(1,0,0.5,1) forwards;
}
.bar .progress-line.new span{
  width: var(--datapercent);
}
.progress-line span::before{
  position: absolute;
  content: "";
  top: -10px;
  right: 0;
  height: 0;
  width: 0;
  border: 7px solid transparent;
  border-bottom-width: 0px;
  border-right-width: 0px;
  border-top-color: #000;
  opacity: 0;
  animation: showText2 0.5s 1.5s linear forwards;
}
.progress-line span::after{
  position: absolute;
  top: -28px;
  right: 0;
  font-weight: 500;
  background: #000;
  color: #fff;
  padding: 1px 8px;
  font-size: 12px;
  border-radius: 3px;
  opacity: 0;
  animation: showText2 0.5s 1.5s linear forwards;
}
@keyframes showText2 {
  100%{
    opacity: 1;
  }
}
.progress-line.new span::after{
  content: var(--contentpercent);
}
.fa-angle-down:hover {
    color: red;
}
.clicker:hover {
    color: red;
}
.container {  
    width:100%;  
}
.container .box {  
    width:100%;
    display:table;  
}  
.container .box .box-row {  
    display:table-row;  
}  
.container .box .box-cell {  
    display:table-cell;  
    width:33%;  
    padding:10px;  
}  
.container .box .box-cell.box1 {  
    text-align:justify;  
}
</style>
<H1 style="text-align:center;">Skillset</H1>
<br>

<div style="text-align: center">
        <div class="container">  
            <div class="box">  
                <div class="box-row">  
                    <div class="box-cell box1">  
                        <div class="skill-bars">
                            <div class="bar">
                                <div class="info">
                                    <span>Programming</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #6665ee">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Gstreamer/OpenCv</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #6665ee">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Build Automation</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #6665ee">
                                    <span></span>
                                </div>
                            </div>
                        </div>    
                    </div>  
                    <div class="box-cell box1">
                        <div class="skill-bars">
                            <div class="bar">
                                <div class="info">
                                    <span>Git</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #66c10b">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Dev.Boards</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #66c10b">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Web Dev.</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #66c10b">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="box-cell box1">  
                        <div class="skill-bars">
                            <div class="bar">
                                <div class="info">
                                    <span>Drone Dev.</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Circuit Designing</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #ea3434">
                                    <span></span>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="info">
                                    <span>Embedded</span>
                                </div>
                                <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #ea3434">
                                    <span></span>
                                </div>
                            </div>
                        </div> 
                    </div>  
                </div>  
            </div>  
        </div> 
</div> 

------------------------------------------------------------------------
<div style="text-align: center">
    <h4>Index</h4>
    <h5>
         <a href="#languages">Languages</a> | <a href="#hardware">Hardware Platforms</a> | <a href="#drone">Drone/UAV Stacks</a> | <a href="#website">Website Development Skills</a> | <a href="#tools">Tools</a> | <a href="#middleware">Middleware Platforms</a>
    </h5>
</div>
<br>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="languages">Languages, Libraries, Build Tools & Version tools</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1">
        <h3>Programming / Scipting Languages</h3>
        <i class="fa fa-angle-down fa-2x"></i>
    </div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>C++</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #6665ee">
                <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>C</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #6665ee">
                <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Python</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #6665ee">
                <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Shell Script</span>
            </div>
            <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #6665ee">
                <span></span>
            </div>
        </div>
    </div>    
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Media/Vision Handling Libraries</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>GStreamer</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>OpenCV</span>
            </div>
            <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Build Automation Tools</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Bitbake</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Makefile</span>
            </div>
            <div class="progress-line new" style="--datapercent: 70%; --contentpercent: '70%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>AutoTools</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>CMakeLists</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #6665ee">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1">
        <h3>Version-control systems</h3>
        <i class="fa fa-angle-down fa-2x"></i>
    </div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>GIT</span>
            </div>
            <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #6665ee">
                <span></span>
            </div>
        </div>
    </div>
</div>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="hardware">Hardware Platforms</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>μController Development Platforms</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Arduino Boards</span>
            </div>
            <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>ESP Based Systems</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>STM32 dev. boards</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>μProcessor Development Platforms</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Raspberry Pi​</span>
            </div>
            <div class="progress-line new" style="--datapercent: 95%; --contentpercent: '95%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Orange pi​</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>other ARM based popular SOC’s</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #66c10b">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="drone">Drone/UAV Stack</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Firmware</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Ardupilot</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>PixHawk</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>betaFlight</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Frame Designs</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Multi-Rotor</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Flying Wing</span>
            </div>
            <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Fixed Wing</span>
            </div>
            <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="website">Website developement Skills</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Front-End Developement</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>HTML</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>CSS</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Back-End Developement</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>PHP</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>MySQL</span>
            </div>
            <div class="progress-line new" style="--datapercent: 70%; --contentpercent: '70%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Python Django</span>
            </div>
            <div class="progress-line new" style="--datapercent: 85%; --contentpercent: '85%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="tools">Tools</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Circuit Designing Tools</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>KiCad</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Eagle (autodesk)</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Debugging Tools</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>gdb</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Valgrind</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>readlelf</span>
            </div>
            <div class="progress-line new" style="--datapercent: 80%; --contentpercent: '80%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Image/Video Editing Tools</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>Adobe Photoshop</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Adobe After Effects</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>Kden-Live</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>

------------------------------------------------------------------------
<H3 style="text-align:center;"><a id="middleware">Middleware Platforms</a></H3>
------------------------------------------------------------------------
<div class="skill-bars">
    <div class="clicker" tabindex="1"><h3>Middleware Platforms</h3><i class="fa fa-angle-down fa-2x"></i></div>
    <div class="hiddendiv">
        <div class="bar">
            <div class="info">
                <span>WPE-Framework</span>
            </div>
            <div class="progress-line new" style="--datapercent: 90%; --contentpercent: '90%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
        <div class="bar">
            <div class="info">
                <span>ROS</span>
            </div>
            <div class="progress-line new" style="--datapercent: 75%; --contentpercent: '75%'; --backgroundColour: #ea3434">
            <span></span>
            </div>
        </div>
    </div>
</div>
<br>