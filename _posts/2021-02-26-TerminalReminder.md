---
layout: inner
position: left
title: 'Terminal Reminder'
date: 2021-02-26
categories: development design
tags: Python3 ShellScripts FOSS
featured_image: '/img/posts/TerminalReminder.png'
project_link: 'https://gitlab.com/vaisakh032/reminder-terminal-app'
button_icon: 'gitlab'
button_text: 'visit project'
project_link2: ''
button_icon2: ''
button_text2: ''
project_link3: ''
button_icon3: ''
button_text3: ''
project_link4: ''
button_icon4: ''
button_text4: ''
lead_text: 'It is a python library that will bring in the reminder support for a terminal session on start of the session.'
---
