---
layout: inner
position: right
title: 'TSlide Mouse'
date: 2021-02-27
categories: development design
tags: Arduino Python3 KiCad FOSS
featured_image: '/img/posts/TSlideMouse.png'
project_link: 'https://gitlab.com/icfoss/Assistive_Tech/t_slide_mouse'
button_icon: 'gitlab'
button_text: 'visit project'
project_link2: 'https://gitlab.com/icfoss/Assistive_Tech/t_slide_mouse/-/blob/master/Design/Tslide_Mouse_Design_Documentation.pdf'
button_icon2: 'file'
button_text2: 'Design Document'
project_link3: ''
button_icon3: ''
button_text3: ''
project_link4: ''
button_icon4: ''
button_text4: ''
lead_text: 'TSlide mouse is a open source USB input device (HID) for Persons with Disabilities (PwD). This repository contains the firmware and configuration files for TSlide Mouse. '
---
