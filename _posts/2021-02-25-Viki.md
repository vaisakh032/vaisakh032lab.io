---
layout: inner
position: right
title: 'Viki - Personal Assistant'
date: 2021-02-25
categories: development design
tags: Python3 ZeroMQ Multi-threaded gstreamer OpenCV FOSS
featured_image: '/img/posts/Viki.png'
project_link: 'https://gitlab.com/vaisakh032/viki/-/tree/master'
button_icon: 'gitlab'
button_text: 'visit project'
project_link2: ''
button_icon2: ''
button_text2: ''
project_link3: ''
button_icon3: ''
button_text3: ''
project_link4: ''
button_icon4: ''
button_text4: ''
lead_text: 'Viki is a personal assistant that can be deployed in your home, which will take care of different tasks. Currently in her development stage, different functionalities are added
to her capability sets each month.'
---
