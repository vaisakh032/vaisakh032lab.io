---
layout: inner
position: right
title: 'NodeMCU Based GoPro Trigger'
date: 2021-03-01
categories: development design
tags: C++ NodeMCU Arduino KiCad FOSS
featured_image: '/img/posts/NodeMCUCircuit.png'
project_link: 'https://gitlab.com/icfoss/drone/nodemcu_gopro_trigger/-/tree/master'
button_icon: 'gitlab'
button_text: 'Visit Project'
project_link2: 'https://gitlab.com/icfoss/drone/nodemcu_gopro_trigger/-/tree/master/circuit'
button_icon2: 'bolt'
button_text2: 'Circuit'
project_link3: ''
button_icon3: ''
button_text3: ''
project_link4: ''
button_icon4: ''
button_text4: ''
lead_text: "NodeMCU based wifi shutter trigger and configuration selector for GoPro camera, to enable camera control for Flight Controller"
---
