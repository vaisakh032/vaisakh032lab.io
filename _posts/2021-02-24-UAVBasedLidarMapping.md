---
layout: inner
position: left
title: 'UAV Based LiDar Mapping'
date: 2021-02-24
categories: development design
tags: C++ Python3 OpenCV ROS RaspberryPi3 SLAM ShellScripts FOSS
featured_image: '/img/posts/UAVLidarMapping.png'
project_link: 'http://www.ijastems.org/wp-content/uploads/2017/04/v3.sia1_.58.UAV-Photogrammetry-and-Lidar-Mapping.pdf'
button_icon: 'print'
button_text: 'Pubished Paper'
project_link2: ''
button_icon2: ''
button_text2: ''
project_link3: ''
button_icon3: ''
button_text3: ''
project_link4: ''
button_icon4: ''
button_text4: ''
lead_text: 'The project aims at designing a Quad-Copter that can fly into a environment and prepare a 2D scaled map as wellas a 3D map of the of its surrounding. The 2D map is blue print of the surrounding which is scanned and plotted by a distance measuring sensor which  uses  pulsed  lase  light.  The  3D  output  is  made  possible  with  the  help  of  photogrammetry  process  of  the  images  captured  by  the camera. '
---
