---
layout: inner
position: left
title: 'FLip Mouse'
date: 2021-03-02
categories: development
tags: Arduino C/C++ Atmega32U4 HID KiCad Python FOSS
featured_image: '/img/posts/FlipMouse.png'
project_link: 'https://gitlab.com/icfoss/Assistive_Tech/flipmouse'
button_icon: 'gitlab'
button_text: 'Visit Project'
project_link2: 'https://gitlab.com/icfoss/Assistive_Tech/flipmouse/-/blob/master/Construction%20manual.pdf'
button_icon2: 'cog'
button_text2: 'Construction Manual'
project_link3: 'https://gitlab.com/icfoss/Assistive_Tech/flipmouse/-/blob/master/User%20manual%20In.pdf'
button_icon3: 'file'
button_text3: 'User Manual'
project_link4: 'https://gitlab.com/icfoss/Assistive_Tech/flipmouse/-/tree/master/Circuit%20Design'
button_icon4: 'print'
button_text4: 'KiCad Design Files'
lead_text: 'The FLipMouse is an open source Assistive Technology module which allows people who cannot use standard computer input devices to control a computer mouse cursor or a joystick as well as typing desired keyboard keys or using infraredremote controls.'
---
