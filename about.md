---
layout: inner
title: About
permalink: /about/
---

<!-- Avatar Image -->
<img class="avatarFixed" src="../img/vaisakh_anand_avatar.jpg" alt="Vaisakh Anand" style="float:right">

<!-- Title Name -->
# {% if site.title != '' %}{{site.title}}{% endif %}

<!-- Contact Deatails -->
{% if site.location != '' %}<i class="fa fa-map-marker"></i> Location: {{site.location}}{% endif %}
<br>
{% if site.telephone != '' %}<i class="fa fa-phone"></i> Phone: <a href="tel:{{site.telephone}}">{{site.telephone}}</a>{% endif %}
<br>
{% if site.email != '' %}<i class="fa fa-envelope-o"></i> Email : <a href="mailto:{{site.email}}">{{site.email}}</a>{% endif %}

<!-- INDEX -->
<br>
<div style="text-align: center">
    <h5>
        <a href="#work-history">Work History</a> | <a href="#education">Education</a> | <a href="#technical-skills">Technical Skills</a>
    </h5>
</div>

## SUMMARY
--------------------------------------------
<div style="text-align: justify">
Highly motivated and innovative software developer with over 5 years of experience seeking to contribute to solving tomorrow's problems. A lifelong learner with a passion for understanding and sharing knowledge, I am dedicated to utilizing my skills and expertise to make a meaningful impact in the field of software development. </div>

## WORK HISTORY
--------------------------------------------
### _Tata Elxsi_
_Philadelphia, US_
> Sr.Software Development Engineer (Full-time)
 (June 2019 - current)

- **Development of OTT apps for RDK**​ :
  - Led the development of device layer code for Linux-based native OTT applications, taking charge of the Amazon
Prime Video port for the RDK community. Also played a key role in supporting development for other premium
applications, such as Netflix, YouTube and Peacock within the RDK ecosystem.
  - Developed and maintained recipes and meta layers for OTT applications for Yocto builds in RDK.
  - Assisted in the development of validation test suites, modeled after those in OTT applications, to catch errors in new
platform releases. Reduced certification time for OTT apps and successfully eliminated regressions in Gstreamer and
related components, making releases market-ready.
  - Played an active role in contributing to the UI and DIAL-related components in RDK, ensuring that all DIAL-related
requirements for OTT applications were met.
  - Collaborated with the platform team to generate OCI bundles and configurations for OTT applications, including
Amazon Prime, in RDK, enabling the seamless use of apps in containers.
- **Development of Testsuite for Platform’s quality of playback**:
  - Led the development of a comprehensive test suite for testing playback quality on various Soc platforms.
  - Designed and developed the test suite to identify frame drops, freezes, audio-video sync delays, macro blocking, and
other playback-related issues that occur during media playback on the platform.
  - The suite help to improve playback quality and a reduction in playback-related issues in releases from SoC vendors.
- **Development of video streaming applications**: Developed Gstreamer-based pipelines for applications that support
EME/MSE standard players for playback of encrypted and non-encrypted content. Experienced in handling DRM
requirements of multiple premium applications with the support of libraries such as <a href="https://github.com/rdkcentral/OCDM-Playready">open-cdm</a>
- **RDK services and WPE-Framework** : Developed several system plugins using Thunder that allowed other plugins in the
platform to operate effectively.
- **Alexa integration** : Collaborated with the team to develop server-level code using Django framework for seamless Alexa
integration into client set-top boxes for IoT applications.

### _International Center for Free and Open-Source Software (ICFOSS)_
_Thiruvananthapuram, Kerala, India_
> Research Assistant (Full-time)
​ (Oct 2018- June 2019)

- **3D Aerial Mapping**:
  - Developed open-source solutions for generating high-quality 3D models of geographic data captured using
point-and-shoot cameras in conjunction with UAV technology.
  - Designed, prototyped, and built a Hexacopter equipped with custom-made trigger circuits to enable
autonomous flight plans for capturing aerial photos using a regular point-and-shoot camera.
  - Developed and implemented Python-based automation scripts to efficiently handle and process large amounts of
data captured during mapping.
- **Open Hardware based solution in Assistive Tech**:
  - Development of solutions for enabling ICT (Information and Communications Technology) for people with disabilities
  - Design and development of circuit and firmware for T-slide Mouse, Kerala’s first ergonomic mouse for persons with
disabilities.
  - Designed and developed circuit and firmware for FLipmouse, a hands-free mouse and keyboard replacement that can
be easily controlled by very low forces applied to the mouthpiece using sip and puff or manual switches, allowing
for greater accessibility for users with disabilities.
  - All products developed are documented here - gitlab.com/icfoss/Assistive Tech

>Research Fellow​
(Oct 2017- Oct 2018)

- **Open-source hardware and Software**: During the research fellowship, gained extensive experience in open-source
software and hardware development, including projects such as OpenCV-based image processing, open-source UAV
design and log diagnostics, 3D photogrammetry utilizing open-source toolkits, and open hardware-based assistive
technology device design and implementation, as well as basic website development using open-source frameworks.

### _Kerala Startup Mission_
_Thiruvananthapuram, Kerala, India_
>Junior Research Fellow​ (March-September 2017)

- **2D LIDAR mapping using quadcopter**: 
  - Design, prototype and build a quadcopter which is capable of flying indoors while carrying a Raspberry and 2D
LiDAR onboard.
  - Develop a ROS based module for capturing the data from the lidar unit and plot them using Rviz in the onboard
Raspberry.

## EDUCATION
---------------------------------------------

### _Lourdes Matha College Of Science and Technology_
_Thiruvananthapuram, Kerala, India_
>B.Tech: Electronics and Communication Engineering​ ​ (2013-17)

- Academic and laboratory knowledge in core subjects of electronics and communication
    - Electives on Image and Speech Processing, VLSI Designs, Digital Coding and Encryption
    - Seminar on Anonymity protocols; why and what is TOR ( ​ THE ONION ROUTING)
    - Main Project on UAV Photogrammetry And LIDAR Mapping
- 7.16​ GPA

### _Dr. GR Public School_
_Thiruvananthapuram, Kerala, India_
>High School: Computer Science stream ​ (2013)

- Computer science stream with mathematics, taking Physical education as an additional subject.


## TECHNICAL SKILLS
---------------------------------------------

|**Programming Languages**             | |**:** C, C++​ , ​ Python​ , PHP, HTML|
|**Operating Systems**                 | |**:** Unix Based systems, MS WIndows |
|**Database**                          | |**:** MySql |
|**Automation Scripting**              | |**:** Python based automation scripting, shell scripts |
|**μController Development Platforms** | |**:** Arduino​ boards, ESP based systems, STM32 dev. boards  |
|**μProcessor Development Platforms**  | |**:** Raspberry Pi​ , ​Orange pi​ and other popular SOC’s|
|**Circuit Designing Tools/Suites**    | |**:** KiCad​ , Eagle (autodesk)|
|**Image Processing**                  | |**:** 3D Photogrammetry tools: Open Drone Maps, Raw Libraries: OpenCV​ - Python|
|**Audio Processing**                  | |**:** Raw Libraries: librosa​ - Python|
|**Robotics middleware Platforms**      | |**:** ROS​ _(Robotic Operating System)_|
|**UAV Build and control Stack**       | |**:** **Ground Station Controls​ :**Mavlink based ​ (Mission Planner) , **Firmware:** Ardupilot​ , betaFlight, pixHawk , **Frame Designs:** Multi-rotors, flying wing, fixed wing|
|**Embedded linux distribution**       | |**:** Yocto |
|**Build Automation tools**       | |**:** Bitbake , CMAKE and AutoTools   |
|**Media Handling libraries**       | |**:** Gstreamer   |
|**Version-control systems**       | |**:** Git|
|**Debugging Tools**       | |**:** gdb, valgrind, readelf|
|**Image and Video editing Tools**       | |**:** Adobe Photoshop, Adobe After Effects|

<!-- ********************************************************************************************** -->
<!-- QR Code to resume -->

{% if site.resume_download_link != '' %}
<div class="qr_with_text" style="float:right">
    <a href="https://{{ site.resume_download_link }}">
        <img src="../img/resume_qr_code.png" alt="resume digital" style="float:right">
        <p>click to download resume&nbsp;&nbsp;</p>
    </a>
</div>
{% endif %}